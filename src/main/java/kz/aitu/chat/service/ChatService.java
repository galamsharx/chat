package kz.aitu.chat.service;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Message;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ChatService {
    private ChatRepository _chatRepository;
    private ParticipantService _participantService;;
    private UsersService _usersService;
    private MessageService _messageService;

    public List<Chat> findAll() {
        return _chatRepository.findAll();
    }

    public Optional<Chat> findById(Long id) {
        return _chatRepository.findById(id);
    }

    public Chat save(Chat chat) {
        return _chatRepository.save(chat);
    }

    public void deleteById(Long id) {
        _chatRepository.deleteById(id);
    }

    public List<Users> getAllUsersByChatId(Long chat_id) {
        List<Participant> participantList = _participantService.findAllByChatId(chat_id);
        List<Users> userList = new ArrayList<>();
        for (Participant participant : participantList) {
            Optional<Users> usersOptional =_usersService.findById(participant.getUserId());
            if (usersOptional.isPresent()) { userList.add(usersOptional.get()); }
        }
        return userList;
    }

    public List<Message> readChat(Long chatId, Long readerId) throws Exception {
        if (!_participantService.existsByChatIdAndUserId(chatId, readerId)) {
            throw new Exception("Forbidden");
        }

        List<Message> notReadMessages = _messageService.getLast20MessagesByUserIdAndChatIdWhereNotRead(chatId, readerId);
        for(Message message: notReadMessages){
            message.setRead(true);
            message.setReadTimestamp(new Date().getTime());
            _messageService.save(message);
        }

        return notReadMessages;
    }
}
