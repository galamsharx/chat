package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.repository.MessageRepository;
import kz.aitu.chat.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.Part;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ParticipantService {
    private ParticipantRepository participantRepository;

    public List<Participant> findAll() {
        return participantRepository.findAll();
    }
    public List<Participant> findAllByChatId(Long chat_id){return participantRepository.findAllByChatId(chat_id);}
    public Optional<Participant> findById(Long id) {
        return participantRepository.findById(id);
    }

    public Participant save(Participant participant) {
        return participantRepository.save(participant);
    }

    public void deleteById(Long id) {
        participantRepository.deleteById(id);
    }

    public boolean existsByChatIdAndUserId(Long chatId,Long userId){
        return participantRepository.existsByChatIdAndUserId(chatId, userId);
    }

}
