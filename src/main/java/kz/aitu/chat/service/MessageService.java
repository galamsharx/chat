package kz.aitu.chat.service;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private MessageRepository messageRepository;
    private ParticipantService participantService;


    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public Optional<Message> findById(Long id) {
        return messageRepository.findById(id);
    }

    public Message addMessage(Message message) throws Exception {
        if (message == null) throw new Exception("message is null");
        if (message.getId() != null) throw new Exception("message has id");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");

        if (!participantService.existsByChatIdAndUserId(message.getChatId(), message.getUserId())) {
            throw new Exception("Forbidden");
        }

        if(message.getText().equals("user starts write message")){
            message.setMessageType("writing");
        } else if (message.getText().equals("user in chat")){
            message.setMessageType("online");
        } else {
            message.setMessageType("basic");
        }

        return messageRepository.save(message);
    }


    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public Message update(Message updatedMessage) throws Exception {
        if (updatedMessage == null) throw new Exception("Message is null");
        if (updatedMessage.getId() == null) throw new Exception("Message id is null");
        if (updatedMessage.getText() == null || updatedMessage.getText().isBlank()) throw new Exception("Message text is blank or null");

        if (!participantService.existsByChatIdAndUserId(updatedMessage.getChatId(), updatedMessage.getUserId())) {
            throw new Exception("Forbidden");
        }

        Optional<Message> messageDb = messageRepository.findById(updatedMessage.getId());
        if (messageDb.isEmpty()) throw new Exception("Message not found");
        updatedMessage.setText(updatedMessage.getText());
        Message message = messageDb.get();
        message.setText(updatedMessage.getText());
        return save(message);
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }

    public List<Message> getLast10MessagesByChatId(Long chat_id, Long reader_id) throws Exception {
        if (!participantService.existsByChatIdAndUserId(chat_id, reader_id)) {
            throw new Exception("Forbidden");
        }
        List<Message> messages = messageRepository.findAllByChatId(chat_id, (Pageable) PageRequest.of(1, 10, Sort.by(Sort.Direction.DESC, "id")).first());
        for (Message message : messages) {
            if (message.getUserId() != reader_id && !message.isDelivered()) {
                message.setDelivered(true);
                message.setDeliveredTimestamp(new Date().getTime());
                save(message);
            }
        }
        return messages;
    }

    public List<Message> getLast10MessagesByUserId(Long user_id) {
        return messageRepository.findFirst10ByUserIdOrderByIdDesc(user_id);
    }

    public List<Message> getLast20MessagesByUserIdAndChatIdWhereNotRead(Long chatId, Long userId) throws Exception {
        if (!participantService.existsByChatIdAndUserId(chatId, userId)) {
            throw new Exception("Forbidden");
        }
        return messageRepository.findAllByChatIdAndAndUserIdIsNotAndIsDeliveredTrueAndIsReadFalse(chatId,userId,(Pageable) PageRequest.of(1, 20, Sort.by(Sort.Direction.DESC, "id")).first());
    }

    public List<Message> getAllNotDeliveredMessagesByChatId(Long chatId, Long userId) throws Exception {
        if (!participantService.existsByChatIdAndUserId(chatId, userId)) {
            throw new Exception("Forbidden");
        }
        return messageRepository.findAllByChatIdAndAndUserIdIsNotAndIsDeliveredFalse(chatId, userId);
    }
}
