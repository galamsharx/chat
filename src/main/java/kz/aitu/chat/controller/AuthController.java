package kz.aitu.chat.controller;


import kz.aitu.chat.DTOs.LoginDTO;
import kz.aitu.chat.DTOs.RegisterDTO;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/auth")
@AllArgsConstructor
public class AuthController {
    private AuthService authService;
    @PostMapping("login")
    public ResponseEntity<?> login(@RequestBody LoginDTO auth) throws Exception {
        return ResponseEntity.ok(authService.Login(auth.getLogin(),auth.getPassword()));
    }

    @PostMapping("register")
    public ResponseEntity<?> register(@RequestBody RegisterDTO auth) throws Exception {
        return ResponseEntity.ok(authService.Register(auth.getLogin(), auth.getPassword(), auth.getName()));
    }
}
