package kz.aitu.chat.controller;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@RestController
@RequestMapping("api/v1/chats")
@AllArgsConstructor
public class ChatController {
    private ChatService chatService;
    private AuthService authService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(chatService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(chatService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<?> addChat(@RequestBody Chat chat, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(chatService.save(chat));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteChat(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        chatService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{chatId}/users")
    public ResponseEntity<?> getAllUsersByChatId(@PathVariable Long chatId, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(chatService.getAllUsersByChatId(chatId));
    }

    @GetMapping("read-chat/{chatId}")
    public ResponseEntity<?> readChatByChatId(@PathVariable Long chatId, @RequestHeader("token") UUID token) throws Exception {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        Long reader_id = authService.getIdByToken(token);
        if (reader_id == null)
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(chatService.readChat(chatId, reader_id));
    }

}
