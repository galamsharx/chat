package kz.aitu.chat.controller;

import kz.aitu.chat.model.Participant;
import kz.aitu.chat.repository.ParticipantRepository;
import kz.aitu.chat.service.AuthService;
import kz.aitu.chat.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/participants")
@AllArgsConstructor
public class ParticipantController {
    private ParticipantService participantService;
    private AuthService authService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(participantService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(participantService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<?> addParticipant(@RequestBody Participant participant, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(participantService.save(participant));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteParticipant(@PathVariable Long id, @RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        participantService.deleteById(id);
        return ResponseEntity.noContent().build();
    }


}
